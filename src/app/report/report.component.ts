import { Component, OnInit } from "@angular/core";
import { GetService } from "./get.service";
import { global } from "../global";
import * as CanvasJS from "./canvasjs.min";
interface task {
  taskID: string;
  command: string;
  startTime: string;
  lastUpdate: string;
  status;
  report;
  displayedColumns: string[];
  outputRows: string[][];
  dataPoints: any[];
}
@Component({
  selector: "app-report",
  templateUrl: "./report.component.html",
  styleUrls: ["./report.component.css"]
})
export class ReportComponent implements OnInit {
  message: string = "Please provide the task ID!";
  sub;
  tasks: task[] = [];
  tasksToDisplay = global.tasksToDisplay;
  dataObjects = [];
  axisX = global.axisX;
  axisY = global.axisY;
  attributes: string[] = [];
  chart;
  data;
  userID = global.userID;
  constructor(private getService: GetService) {}
  ngOnInit() {
    this.tasks = [];
    global.tasksToDisplay.forEach(taskID => {
      this.getTask(taskID).then(task => {
        // console.log(task);
        this.tasks.push(task);
      });
    });
    setTimeout(() => {
      this.updateChart();
    }, 500);
  }

  addCountToSeries(chart) {
    for (var i = 0; i < chart.options.data.length; i++) {
      chart.options.data[i].clickCount = 0;
    }
  }

  itemclick(e) {
    ++e.dataSeries.clickCount;
    for (var i = 0; i < e.chart.options.data.length; i++) {
      if (e.dataSeries.clickCount % 2 === 0) {
        e.chart.options.data[i].visible = true;
      } else {
        if (e.dataSeries !== e.chart.options.data[i]) {
          e.chart.options.data[i].visible = false;
          e.chart.options.data[i].clickCount = 0;
        } else {
          e.dataSeries.visible = true;
        }
      }
    }
    e.chart.render();
  }

  updateChart() {
    this.prepareData();
    this.chart = new CanvasJS.Chart("chartContainer", {
      exportEnabled: true,
      animationEnabled: true,
      zoomEnabled: true,
      title: {
        text: "Live Chart"
      },
      axisX: {
        title: global.axisX
      },
      axisY: {
        title: global.axisY
      },
      legend: {
        cursor: "pointer",
        itemclick: this.itemclick
      },
      data: this.data
    });
    this.addCountToSeries(this.chart);
    this.chart.render();
  }

  prepareData() {
    this.data = [];
    for (let index = 0; index < this.tasks.length; index++) {
      let task = this.tasks[index];
      console.log(task);
      console.log(this.tasks[index].dataPoints);
      let dataObject = {
        type: "spline",
        showInLegend: true,
        name: task.taskID,
        legendText: task.taskID,
        dataPoints: this.tasks[index].dataPoints
      };
      this.data.push(dataObject);
    }
  }

  updateReport() {
    global.axisX = this.axisX;
    global.axisY = this.axisY;
    this.ngOnInit();
  }

  async getTask(taskID: string): Promise<task> {
    // console.log(taskID);
    let task: task = {
      taskID: taskID,
      command: "",
      startTime: "",
      lastUpdate: "",
      status: null,
      report: null,
      displayedColumns: [],
      outputRows: [],
      dataPoints: []
    };
    if (taskID) {
      this.getService.getReport(taskID).subscribe(msg => {
        // console.log(msg);
        this.message = msg;
        // console.log(this.getService.status);
        task.status = this.getService.status;
        task.report = this.getService.report;
        // console.log(this.getService.startTime);
        task.startTime = this.getService.startTime;
        task.lastUpdate = this.getService.lastUpdate;
        task.command = this.getService.command;

        if (task.report) {
          let splitReport = task.report.split(/\r?\n/);
          task.displayedColumns = splitReport[0].split(",");

          let outputRows = [];
          for (
            let outputIndex = 1;
            outputIndex < splitReport.length;
            outputIndex++
          ) {
            let outputRow = splitReport[outputIndex];

            if (outputRow == "") {
              break;
            }
            let outputRowData = outputRow.split(",");
            outputRows.push(outputRowData);
          }
          task.outputRows = null;
          task.outputRows = outputRows;
          this.roundOutput(task);
          // console.log(task.outputRows);
        }
        if (task.displayedColumns) {
          // tslint:disable-next-line: prefer-for-of
          for (
            let columnIndex = 0;
            columnIndex < task.displayedColumns.length;
            columnIndex++
          ) {
            const column = task.displayedColumns[columnIndex];
            if (this.attributes.includes(column)) {
              continue;
            } else {
              this.attributes.push(column);
            }
          }
          console.log(this.attributes);
          global.attributesDisplaying = task.displayedColumns;
          //Find the index for classified instances

          let classifiedInstancesIndex = -2019;
          let classificationPercentageInstancesIndex = -2019;
          for (let index = 0; index < task.displayedColumns.length; index++) {
            const element = task.displayedColumns[index];
            if (element == this.axisX) {
              classifiedInstancesIndex = index;
            }
            if (element == this.axisY) {
              classificationPercentageInstancesIndex = index;
            }
          }
          task.dataPoints = [];
          for (let index = 0; index < task.outputRows.length; index++) {
            const taskOutput = task.outputRows[index];
            task.dataPoints.push({
              x: parseInt(taskOutput[classifiedInstancesIndex]),
              y: parseInt(taskOutput[classificationPercentageInstancesIndex])
            });
          }
          // console.log(
          //   "Data Points: " + task.dataPoints[0].x + " " + task.dataPoints[0].y
          // );
        }
      });
      return task;
    }
    // console.log(taskID);
    else {
      console.error("No task ID!");
    }
  }
  roundOutput(task: task) {
    for (let index = 0; index < task.outputRows.length; index++) {
      let row = task.outputRows[index];
      for (let cellIndex = 0; cellIndex < row.length; cellIndex++) {
        const cellString = row[cellIndex];
        let rowNum = parseFloat(cellString).toFixed(global.significance);
        row[cellIndex] = rowNum;
      }
      task.outputRows[index] = row;
    }
  }
}
