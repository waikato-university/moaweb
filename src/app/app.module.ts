import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { InputFormComponent } from "./input-form/input-form.component";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FlexModule } from "@angular/flex-layout";
import { MatCardModule } from "@angular/material/card";
import { BrowserModule } from "@angular/platform-browser";
import {
  MatButtonModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule
} from "@angular/material";
import { HttpClientModule } from "@angular/common/http";
import { MainPageComponent } from "./main-page/main-page.component";
import { LayoutModule } from "@angular/cdk/layout";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatIconModule } from "@angular/material/icon";
import { MatListModule } from "@angular/material/list";
import { AppRoutingModule, routingComponents } from "./app-routing.module";
import { UploaderComponent } from "./uploader/uploader.component";
import { MessageService } from "./message.service";
import { LandingComponent } from "./landing/landing.component";
import { MatGridListModule } from "@angular/material/grid-list";

import { SystemMessageBarComponent } from "./main-page/system-message-bar/system-message-bar.component";
import { SettingComponent } from "./main-page/setting/setting.component";
import { MatSelectModule } from "@angular/material/select";
@NgModule({
  declarations: [
    AppComponent,
    InputFormComponent,
    MainPageComponent,
    routingComponents,
    UploaderComponent,
    LandingComponent,
    SystemMessageBarComponent,
    SettingComponent
  ],
  imports: [
    BrowserModule,
    MatButtonModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    FlexModule,
    MatCardModule,
    HttpClientModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    AppRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatGridListModule,
    MatSelectModule
  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule {}
