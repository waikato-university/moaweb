import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-system-message-bar",
  templateUrl: "./system-message-bar.component.html",
  styleUrls: ["./system-message-bar.component.css"]
})
export class SystemMessageBarComponent implements OnInit {
  @Input() message: string = "Welcome to Massive Online Analysis!";
  constructor() {}

  ngOnInit() {}
}
