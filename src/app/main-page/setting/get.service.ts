import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpEvent,
  HttpEventType,
  HttpErrorResponse
} from "@angular/common/http";
import { MessageService } from "src/app/message.service";
import { global } from "../../global";
import { map, tap, last, catchError } from "rxjs/operators";
import { of } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class GetService {
  constructor(private http: HttpClient, private messenger: MessageService) {}
  getUserID() {
    console.log("global.email is : " + global.email);
    return this.http
      .post(
        global.serverAddress + "user",
        { email: global.email },
        { observe: "response", reportProgress: true }
      )
      .pipe(
        map(event => this.getEventMessage(event)),
        tap(message => this.showProgress(message)),
        last(), // return last (completed) message to caller
        catchError(this.handleError())
      );
  }
  private getEventMessage(event: HttpEvent<any>) {
    switch (event.type) {
      case HttpEventType.Sent:
        return `Get request sent for user.`;

      case HttpEventType.Response:
        // console.log(event.body);
        global.userID = event.body.sub;
        global.email = event.body.email;
        global.picture = event.body.picture;
        // console.log(this.status);
        // console.log(this.report);
        return `UserID returned!`;

      default:
        return `UserID get request error surprising event: ${event.type}.`;
    }
  }
  private showProgress(message: string) {
    this.messenger.add(message);
  }
  private handleError() {
    const userMessage = `Getting userID failed. Please provide valid email address!`;

    return (error: HttpErrorResponse) => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      const message =
        error.error instanceof Error
          ? error.error.message
          : `server returned code ${error.status} with body "${error.error}"`;

      this.messenger.add(`${userMessage} ${message}`);

      // Let app keep running but indicate failure.
      return of(userMessage);
    };
  }
}
