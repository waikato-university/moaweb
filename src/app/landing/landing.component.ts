import { Component, OnInit } from "@angular/core";
import { global } from "../global";
@Component({
  selector: "app-landing",
  templateUrl: "./landing.component.html",
  styleUrls: ["./landing.component.css"]
})
export class LandingComponent implements OnInit {
  constructor() {}
  serverAddress = global.serverAddress;
  ngOnInit() {}
  setServerAddress(serverIP: string) {
    if (serverIP) {
      global.serverAddress = serverIP;
      console.log("Current serverIP: " + global.serverAddress);
      if (!global.servers.includes(serverIP)) {
        global.servers.push(serverIP);
        console.log("Servers: " + global.servers);
      }
    }
  }
}
