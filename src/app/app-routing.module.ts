import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { UploaderComponent } from "./uploader/uploader.component";
import { TaskComponent } from "./task/task.component";
import { ReportComponent } from "./report/report.component";
import { LandingComponent } from "./landing/landing.component";
import { MainPageComponent } from "./main-page/main-page.component";
import { SettingComponent } from "./main-page/setting/setting.component";
const routes: Routes = [
  { path: "", component: LandingComponent },
  { path: "dataset", component: UploaderComponent },
  { path: "task", component: TaskComponent },
  { path: "report", component: ReportComponent },
  { path: "setting", component: SettingComponent },
  { path: "main", component: MainPageComponent }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
export const routingComponents = [
  UploaderComponent,
  TaskComponent,
  ReportComponent
];
