import { TestBed, async } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { InputFormComponent } from "./input-form/input-form.component";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FlexModule } from "@angular/flex-layout";
import { MatCardModule } from "@angular/material/card";
import {
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatSelectModule
} from "@angular/material";
import { HttpClientModule } from "@angular/common/http";
import { MainPageComponent } from "./main-page/main-page.component";
import { routingComponents, AppRoutingModule } from "./app-routing.module";
import { UploaderComponent } from "./uploader/uploader.component";
import { LandingComponent } from "./landing/landing.component";
import { LayoutModule } from "@angular/cdk/layout";
import { SystemMessageBarComponent } from "./main-page/system-message-bar/system-message-bar.component";
import { SettingComponent } from "./main-page/setting/setting.component";
describe("AppComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        InputFormComponent,
        MainPageComponent,
        routingComponents,
        UploaderComponent,
        LandingComponent,
        SystemMessageBarComponent,
        SettingComponent
      ],
      imports: [
        MatButtonModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatFormFieldModule,
        FlexModule,
        MatCardModule,
        HttpClientModule,
        LayoutModule,
        MatToolbarModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        AppRoutingModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatSelectModule
      ]
    }).compileComponents();
  }));

  it("should create the app", () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
