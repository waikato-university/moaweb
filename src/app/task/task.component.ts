import { Component, OnInit } from "@angular/core";
import { PostService } from "./post.service";
import { global } from "../global";
import { interval } from "rxjs";
import { Router } from "@angular/router";

interface Command {
  userID: string;
  task: string;
}

interface task {
  taskID: string;
  command: string;
  startTime: string;
  lastUpdate: string;
  status;
  report;
  onDisplay: boolean;
}

@Component({
  selector: "app-task",
  templateUrl: "./task.component.html",
  styleUrls: ["./task.component.css"]
})
export class TaskComponent implements OnInit {
  constructor(private postService: PostService, private router: Router) {
    this.command.task = null;
    this.command.userID = null;
  }
  userID = global.userID;
  tasks: task[] = global.tasks;
  taskID: string = null;
  message: string = "Please provide MOA commands!";
  sub;
  displayedColumns: string[] = [
    "taskID",
    "command",
    "startTime",
    "lastUpdate",
    "status"
  ];
  ngOnInit() {
    if (global.frequency > 0) {
      this.sub = interval(global.frequency * 1000).subscribe(val => {
        this.getTasks();
      });
    }
  }

  command: Command = { userID: null, task: null };
  postCommands(moaCommands: string) {
    if (!moaCommands) {
      console.error("No moa commands!");
      this.message = "Please provide valid MOA commands!";
    }
    // console.log(moaCommands);
    this.postService.postCommands(moaCommands).subscribe(msg => {
      this.message = msg;
      this.taskID = this.postService.taskID;
    });
    this.getTasks();
  }
  getTasks() {
    // console.log("get task history!");
    if (!global.userID) {
      console.error("No user ID!");
      this.message = "No user ID provided! Please upload dataset first.";
    } else {
      this.postService.getTaskHistory().subscribe(msg => {
        this.message = msg;
        // console.log(global.tasks);
        this.tasks = global.tasks;
        this.tasks.forEach(task => {
          console.log(this.briefTaskStatus(task.status));
          task.status = this.briefTaskStatus(task.status);
        });
        this.ngOnInit();
      });
    }
  }
  /**
   * @param {string} taskStatus The task status of a MOA task
   */
  briefTaskStatus(taskStatus: string) {
    if (taskStatus) {
      if (taskStatus.includes("Task completed")) {
        return "Completed";
      } else if (taskStatus.includes("java.lang.Exception")) {
        return "Failed";
      } else if (taskStatus.includes("running")) {
        return "Running";
      }
    } else {
      return "N/A.";
    }
  }
  //Go the report page with current report at display set as taskID
  goToReport(taskID: string) {
    //Set current taskID
    global.currentTaskID = taskID;
    // console.log("Current task ID on display: " + global.currentTaskID);
    //Go to report tab
    this.router.navigate(["/report"]);
  }

  toggleReport(task: task) {
    console.log(global.tasksToDisplay);
    console.log(global.tasksToDisplay.includes(task.taskID));
    if (global.tasksToDisplay.includes(task.taskID)) {
      console.log("Task is in display");
      const index = global.tasksToDisplay.indexOf(task.taskID);
      if (index > -1) {
        global.tasksToDisplay.splice(index, 1);
      }
      console.log(global.tasksToDisplay);
      task.onDisplay = false;
    } else {
      global.tasksToDisplay.push(task.taskID);
      console.log("Task is not in display");
      console.log(global.tasksToDisplay);
      task.onDisplay = true;
    }
  }
}
