import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpEvent,
  HttpEventType,
  HttpErrorResponse
} from "@angular/common/http";
import { of } from "rxjs";
import { catchError, last, map, tap } from "rxjs/operators";
import { MessageService } from "../message.service";
import { global } from "../global";
interface Command {
  userID: string;
  task: string;
}
interface task {
  taskID: string;
  command: string;
  startTime: string;
  lastUpdate: string;
  status: any;
  report: any;
}
@Injectable({
  providedIn: "root"
})
export class PostService {
  taskID: string;
  tasks: task[];
  constructor(private http: HttpClient, private messenger: MessageService) {}
  postCommands(moaCommands: string) {
    if (!moaCommands) {
      return;
    }
    // console.log(command);
    // console.log(moaCommands);
    // console.log(global.userID);
    return this.http
      .post(
        global.serverAddress + "moa",
        { task: moaCommands, userID: global.userID },
        {
          reportProgress: true,
          observe: "response"
        }
      )
      .pipe(
        map(event => this.getEventMessage(event, moaCommands)),
        tap(message => this.showProgress(message)),
        last(), // return last (completed) message to caller
        catchError(this.handleError(moaCommands))
      );
  }
  getTaskHistory() {
    if (!global.userID) {
      console.log("No user ID!");
    } else {
      console.log(global.userID);
      return this.http
        .post(
          global.serverAddress + "history",
          { userID: global.userID },
          {
            reportProgress: true,
            observe: "response"
          }
        )
        .pipe(
          map(event => this.getEventHistoryMessage(event, global.userID)),
          tap(message => this.showProgress(message)),
          last(), // return last (completed) message to caller
          catchError(this.handleError(global.userID))
        );
    }
  }
  private getEventHistoryMessage(event: HttpEvent<any>, userID: string) {
    switch (event.type) {
      case HttpEventType.Sent:
        return `Asking for task history of userID: "${userID}".`;

      case HttpEventType.UploadProgress:
        // Compute and show the % done:
        const percentDone = Math.round((100 * event.loaded) / event.total);
        return `Getting user "${userID}" is ${percentDone}% posting.`;

      case HttpEventType.Response:
        global.tasks = event.body;
        global.tasks.forEach(task => {
          task.status = task.status.code;
        });
        return `Fetched tasks for user "${userID}"`;

      default:
        return `Getting tasks of user "${userID}" has a surprising event: ${event.type}.`;
    }
  }
  private getEventMessage(event: HttpEvent<any>, moaCommands: string) {
    switch (event.type) {
      case HttpEventType.Sent:
        return `Posted "${moaCommands}".`;

      case HttpEventType.UploadProgress:
        // Compute and show the % done:
        const percentDone = Math.round((100 * event.loaded) / event.total);
        return `command "${moaCommands}" is ${percentDone}% posting.`;

      case HttpEventType.Response:
        this.taskID = event.body.taskID;
        return `command "${moaCommands}" sent! Your Task ID is:`;

      default:
        return `command "${moaCommands}" surprising upload event: ${event.type}.`;
    }
  }
  private showProgress(message: string) {
    this.messenger.add(message);
  }
  private handleError(command: string) {
    const userMessage = `${command} post failed.`;

    return (error: HttpErrorResponse) => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      const message =
        error.error instanceof Error
          ? error.error.message
          : `server returned code ${error.status} with body "${error.error}"`;

      this.messenger.add(`${userMessage} ${message}`);

      // Let app keep running but indicate failure.
      return of(userMessage);
    };
  }
}
