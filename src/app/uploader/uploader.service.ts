import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpEvent,
  HttpEventType,
  HttpProgressEvent,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";

import { of } from "rxjs";
import { catchError, last, map, tap } from "rxjs/operators";

import { MessageService } from "../message.service";
import { global } from "../global";
@Injectable()
export class UploaderService {
  constructor(private http: HttpClient, private messenger: MessageService) {}

  // If uploading multiple files, change to:
  // upload(files: FileList) {
  //   const formData = new FormData();
  //   files.forEach(f => formData.append(f.name, f));
  //   new HttpRequest('POST', '/upload/file', formData, {reportProgress: true});
  //   ...
  // }
  upload(file: File, md5: string) {
    if (!file) {
      return;
    }
    let formData: FormData = new FormData();
    formData.set("md5", md5);
    formData.set("userID", global.userID);
    formData.append("dataset", file, file.name);
    console.log(formData);

    return this.http
      .post(global.serverAddress, formData, {
        reportProgress: true,
        observe: "response"
      })
      .pipe(
        map(event => this.getEventMessage(event, file)),
        tap(message => this.showProgress(message)),
        last(), // return last (completed) message to caller
        catchError(this.handleError(file))
      );
  }

  /** Return distinct message for sent, upload progress, & response events */
  private getEventMessage(event: HttpEvent<any>, file: File) {
    switch (event.type) {
      case HttpEventType.Sent:
        return `Uploading file "${file.name}" of size ${file.size}.`;

      case HttpEventType.UploadProgress:
        // Compute and show the % done:
        const percentDone = Math.round((100 * event.loaded) / event.total);
        return `File "${file.name}" is ${percentDone}% uploaded.`;

      case HttpEventType.Response:
        return `File "${file.name}" was completely uploaded!`;

      default:
        return `File "${file.name}" surprising upload event: ${event.type}.`;
    }
  }

  /**
   * Returns a function that handles Http upload failures.
   * @param file - File object for file being uploaded
   *
   * When no `UploadInterceptor` and no server,
   * you'll end up here in the error handler.
   */
  private handleError(file: File) {
    const userMessage = `${file.name} upload failed.`;

    return (error: HttpErrorResponse) => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      const message =
        error.error instanceof Error
          ? error.error.message
          : `server returned code ${error.status} with body "${error.error}"`;

      this.messenger.add(`${userMessage} ${message}`);

      // Let app keep running but indicate failure.
      return of(userMessage);
    };
  }

  private showProgress(message: string) {
    this.messenger.add(message);
  }

  getDatasets() {
    if (!global.userID) {
      console.log("No user ID!");
    } else {
      console.log(global.userID);
      return this.http
        .post(
          global.serverAddress + "dataset",
          { userID: global.userID },
          {
            reportProgress: true,
            observe: "response"
          }
        )
        .pipe(
          map(event => this.getEventHistoryMessage(event, global.userID)),
          tap(message => this.showProgress(message)),
          last(), // return last (completed) message to caller
          catchError(this.handleHistoryError(global.userID))
        );
    }
  }
  private getEventHistoryMessage(event: HttpEvent<any>, moaCommands: string) {
    switch (event.type) {
      case HttpEventType.Sent:
        return `Posted "${moaCommands}".`;

      case HttpEventType.UploadProgress:
        // Compute and show the % done:
        const percentDone = Math.round((100 * event.loaded) / event.total);
        return `command "${moaCommands}" is ${percentDone}% posting.`;

      case HttpEventType.Response:
        console.log(event.body);
        global.datasets = event.body.datasets;
        return `Dataset history request sent!`;

      default:
        return `command "${moaCommands}" surprising upload event: ${event.type}.`;
    }
  }
  private handleHistoryError(command: string) {
    const userMessage = `${command} post failed.`;

    return (error: HttpErrorResponse) => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      const message =
        error.error instanceof Error
          ? error.error.message
          : `server returned code ${error.status} with body "${error.error}"`;

      this.messenger.add(`${userMessage} ${message}`);

      // Let app keep running but indicate failure.
      return of(userMessage);
    };
  }
}
