# Wekaweb

## Prerequisite
### Software
* Docker
* Node.js

## Setup
* Clone moaserver repo
* Run `docker-compose up --build` under the moaserver root directory
* When docker images are up, run `ng serve -o`